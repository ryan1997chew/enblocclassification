import pandas as pd
import os
import config as config
from rea_python.main.database import RedshiftHook
from rea_python.constants import OutputFormat

sql_path = os.path.join(os.path.dirname(os.path.realpath('EnblocImbClass.ipynb')), 'sql')

# initialize redshift hook
src_hook = RedshiftHook(iam_role_arn=config.IAM_ROLE_ARN,
                        via_s3_bucket=config.REDSHIFT_HOOK_BRIDGE_BUCKET,
                        via_s3_folder=config.REDSHIFT_HOOK_BRIDGE_FOLDER)

# set up connection
src_hook.set_conn_from_uri(config.DATA_WAREHOUSE_URI)

# load queries from sql
src_hook.load_queries_from_folders([sql_path])

quick_query = lambda query : src_hook.execute_loaded_query(query_name=query, output_format=OutputFormat.pandas)
c1, c2, c3 = quick_query('c1'), quick_query('c2'), quick_query('c3')
l1, l2, l3 = quick_query('l1'), quick_query('l2'), quick_query('l3')

c_ovr = pd.DataFrame({
                       'project_name': c1.project_name, 
                       'enbloc': c1.enbloc,
                       'c1_final_average': c1.final_average,
                       'c2_final_average': c2.final_average,
                       'c3_final_average': c3.final_average,
                       'overall_average' :(c1.final_average + c2.final_average + c3.final_average)/3   
                       })

l_ovr = pd.DataFrame({
                       'project_name': l1.project_name, 
                       'enbloc': l1.enbloc,
                       'l1_final_average': l1.final_average,
                       'l2_final_average': l2.final_average,
                       'l3_final_average': l3.final_average,
                       'overall_average' :(l1.final_average + l2.final_average + l3.final_average)/3   
                       })
                       
#export to excel
excel_path = os.path.join(os.path.join(os.path.dirname(os.path.dirname(os.path.realpath('EnblocImbClass.ipynb'))), 'input'), 'enbloc_predictions.xlsx')

with pd.ExcelWriter(excel_path, mode='a') as writer:  
    c1.to_excel(writer, sheet_name='condo_1')
    c2.to_excel(writer, sheet_name='condo_2')
    c3.to_excel(writer, sheet_name='condo_3')
    l1.to_excel(writer, sheet_name='landed_1')
    l2.to_excel(writer, sheet_name='landed_2')
    l3.to_excel(writer, sheet_name='landed_3')
    c_ovr.to_excel(writer, sheet_name='condo_overall')
    l_ovr.to_excel(writer, sheet_name='landed_overall')
