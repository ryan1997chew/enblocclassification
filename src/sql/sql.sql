-- name: final
with x as (
select
                project_name,
                cast(project_age as int4) as project_age,
                case
                	when len(postal_code) = 5 then '0' + cast(postal_code as  varchar)
                	when len(postal_code) = 6 then  cast(postal_code as  varchar)
                end as postal_code,
                replace(lower(region),'-',' ') as region,
                lower(zone) as zone,
                property_type_group,
                number_of_units as num_of_units,
                lower(lease_type) as lease_type ,
                approx_project_gfa,
                avg_area_sqft,
                plot_ratio,
                potential_gfa,
                enbloc
from
                data_science.enbloc_processed_enbloc_sg
union all
select
                project_name,
                cast(project_age as int4) as project_age,
                cast(postal_code as varchar) as postal_code,
                replace(lower(region),'-',' ') as region,
                zone,
                replace(property_type_group,'condominium','condo'),
                num_of_units,
                lower(lease_type) as lease_type,
                approx_project_gfa,
                avg_area_sqft,
                cast(plot_ratio as float4) as plot_ratio ,
                cast(potential_gfa as float4) as potential_gfa ,
                enbloc
from
                data_science.enbloc_processed_non_enbloc_sg
),
v as (
select
	avg(latitude) as latitude,
	avg(longitude) as longitude,
	avg(km_to_sg_cbd) as km_to_sg_cbd,
	avg(lot_size_sqm/10.7639) as lot_size_sqft,
	postal_code
from
	datamart.view_address_expand
group by
	postal_code)
select
	v.latitude,
	v.longitude,
	v.km_to_sg_cbd,
	v.lot_size_sqft,
	x.*
from
	x
	left join v on x.postal_code = v.postal_code
order by
	enbloc desc, project_name
;

-- name: enbloc
with base2 as(
with base as(
select
                sale_date,
               cast(postal_code as varchar) as postal_code,
                postal_district,
                postal_sector ,
                planning_region,
                planning_area,
                project_name,
                address,
                property_type,
                case
                                when property_type = 'Terrace House' then 'Landed'
                                when property_type = 'Semi-Detached House' then 'Landed'
                                when property_type = 'Detached House' then 'Landed'
                                when property_type = 'Apartment' or property_type = 'Condominium' then 'Condo'
                end as property_type_group,
                number_of_units,
                transacted_price,
                area_sqft,
                unit_price_psf,
                case
                                when tenure is null then null
                                when cast(substring(tenure,1,charindex(' ',tenure)) as int)>99 or tenure = 'Freehold' then 'Freehold'
                else 'Leasehold' end as lease_type,
                substring(tenure,1,charindex(' ',tenure)) as years_left_proxy,
                case
                                when years_left_proxy < 200 or years_left_proxy = 99 then years_left_proxy
                end as years_left,
                completion_date,
                case
                                when lease_type = 'Leasehold' then right(tenure,4)
                end as tenure_start_date,
                case
                                when lease_type = 'Leasehold' then tenure_start_date - right(sale_date,4) + cast(years_left as int)
                end as years_left_on_tenure
from
                data_science.enbloc_raw_ura
where
                property_type is not null and
                postal_code  is not null and
                project_name  is not null)
select
                --cast(left(sale_date,charindex('/',sale_date)-1) as int) as sale_day,
                cast(right(b.sale_date,4) as int) as sale_year,
                b.completion_date,
                cast(replace(left(substring(sale_date,charindex('/',sale_date)+1),2),'/','') as int) as sale_month,
                b.postal_code,
                b.planning_region,
                b.planning_area,
                b.project_name,
                --b.address,
                b.property_type,
                b.property_type_group,
                b.number_of_units,
                b.transacted_price,
                b.area_sqft,
                b.area_sqft/b.number_of_units as avg_unit_area_sqft,
                b.unit_price_psf,
                b.lease_type,
                b.years_left_on_tenure,
                r.plot_ratio,
                r.potential_gfa,
                1 as enbloc
from
                base b
                left join data_science.enbloc_raw_source_sg r on b.project_name = r.project_name)
select
                avg(sale_year) as sale_year,
                cast(avg(sale_year) - avg(completion_date) as int4) as project_age,
                avg(sale_month) as sale_month,
                project_name ,
                cast(avg(postal_code) as varchar) as postal_code ,
                planning_region as region,
                planning_area as zone ,
                lower(property_type_group) as property_type_group,
                sum(number_of_units) as number_of_units,
                lease_type,
                avg(years_left_on_tenure) as years_left_on_tenure,
                cast(sum(area_sqft) as int) as  approx_project_gfa,
                cast(avg(avg_unit_area_sqft) as int) as avg_area_sqft,
                avg(unit_price_psf) as unit_price_psf,
                avg(plot_ratio) as plot_ratio,
                avg(potential_gfa) as potential_gfa,
                enbloc
from
                base2
group by
                project_name, planning_region,planning_area, lease_type, property_type_group,enbloc
order by
                sale_year,
                sale_month
                ;

-- name: non_enbloc
with cte1 as
(select
                dw_project_id,
                cast(sum(area_sqft_inferred)as int) as approx_project_gfa,
                cast(avg(area_sqft_inferred) as int) as avg_area_sqft,
                count(*) as num_of_units
from
                datamart.view_property_expand
where
                property_data_type = 'SINGLE'
                and country = 'sg'
                and (property_type_group = 'Condo' or property_type_group = 'Landed')
group by
                dw_project_id),
cte2 as (
select
                dw_project_id,
                upper(p.project_name) as project_name,
                2021 - p.completion_year as project_age,
                a.postal_code,
                a.region,
                a.zone,
                p.project_property_type as property_type_group,
                p.num_of_units as number_of_units,
                case
                                when p.tenure is null then null
                                when p.tenure = 'Freehold' then 'Freehold'
                                when len(left(p.tenure,charindex('Y',tenure)-2)) = 3 and left(p.tenure,1) = 9 then 'Freehold'
                                else 'Leasehold'
                end as lease_type,
                cast(a.lot_size_sqm/10.7639 as int) as lot_size_sqft
from
                datamart.view_project_expand p
                inner join datamart.view_address_expand a on p.dw_address_id = a.dw_address_id
where
                p.country = 'sg'
                and (p.project_property_type = 'condominium' or p.project_property_type = 'landed')
                and p.dw_address_id is not null)
select
                cte2.project_name,
                cte2.project_age,
                cte2.postal_code,
                cte2.region,
                cte2.zone,
                cte2.property_type_group,
                COALESCE(cte2.number_of_units, cte1.num_of_units) as  num_of_units,
                cte2.lease_type,
                cte1.approx_project_gfa,
                cte1.avg_area_sqft,
                cte2.lot_size_sqft,
                null as plot_ratio,
                null as potential_gfa,
                0 as enbloc
from
                cte2
                left join cte1 using (dw_project_id)
;

-- name: data_query
select
	*,
	case
		when enbloc = '0' then 'enbloc'
		when enbloc = '1' then 'nonenbloc'
	end as categorical_enbloc
from
	data_science.enbloc_processed_final_sg
order by
	num_of_units desc;

-- name: c1
select
	*
from
	data_science.enbloc_predictions_condo_1
where
	enbloc = 0
order by
	project_name desc;

-- name: c2
select
	*
from
	data_science.enbloc_predictions_condo_2
where
	enbloc = 0
order by
	project_name desc;

-- name: c3
select
	*
from
	data_science.enbloc_predictions_condo_3
where
	enbloc = 0
order by
	project_name desc;

-- name: l1
select
	*
from
	data_science.enbloc_predictions_landed_1
where
	enbloc = 0
order by
	project_name desc;

-- name: l2
select
	*
from
	data_science.enbloc_predictions_landed_2
where
	enbloc = 0
order by
	project_name desc;

-- name: l3
select
	*
from
	data_science.enbloc_predictions_landed_3
where
	enbloc = 0
order by
	project_name desc;