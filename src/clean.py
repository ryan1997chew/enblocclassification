'''
This script will return:

df - the design matrix for PyCaret to use
x - the design matrix minus the target, for predicting probabilities, for a specific model to use
names - names of the condos/landed
enbloc - the true labels of the condos/landed

We can use the function 'make_results' in the other scripts.

Prefixes - prefaced by 'c' or 'l' for condo or landed - so 'cdf' for condo df.

Since PyCaret setup cannot work for more than one setup (one environment for condo and landed), we have to train each setup separately.

'''

import pandas as pd
import os
import config as config
from rea_python.main.database import RedshiftHook
from rea_python.constants import OutputFormat
from sklearn.preprocessing import PowerTransformer
from sklearn.cluster import KMeans, AgglomerativeClustering
from sklearn.mixture import GaussianMixture

sql_path = os.path.join(os.path.dirname(os.path.realpath('EnblocImbClass.ipynb')), 'sql')

# initialize redshift hook
src_hook = RedshiftHook(iam_role_arn=config.IAM_ROLE_ARN,
                        via_s3_bucket=config.REDSHIFT_HOOK_BRIDGE_BUCKET,
                        via_s3_folder=config.REDSHIFT_HOOK_BRIDGE_FOLDER)

# set up connection
src_hook.set_conn_from_uri(config.DATA_WAREHOUSE_URI)

# load queries from sql
src_hook.load_queries_from_folders([sql_path])
df = src_hook.execute_loaded_query(query_name='data_query', output_format=OutputFormat.pandas)

# need to fill in the missing PropertyGuru benchmark properties so they don't get dropped
df.lease_type[df.project_name == 'BRADDELL VIEW'] = 'leasehold'
df.lease_type[df.project_name == 'HILLCREST ARCADIA'] = 'leasehold'

# split and drop and create new feature
enbloc_df = df[df['enbloc'] == 1]
nonenbloc_df = df[df['enbloc'] == 0]
condo_df = df[df['property_type_group'] == 'condo']
landed_df = df[df['property_type_group'] == 'landed']

condo_df['lot_size_num_units_ratio'] = condo_df.lot_size_sqft / condo_df.num_of_units
landed_df['lot_size_num_units_ratio'] = landed_df.lot_size_sqft / landed_df.num_of_units
condo_df.dropna(subset=['lease_type', 'approx_project_gfa', 'avg_area_sqft', 'region', 'zone', 'num_of_units'],
                inplace=True)
landed_df.dropna(subset=['lease_type', 'approx_project_gfa', 'avg_area_sqft', 'region', 'zone', 'num_of_units'],
                 inplace=True)


def preprocess(df, cols, dummies):
    # dropna
    df.dropna(subset=['lease_type', 'approx_project_gfa', 'avg_area_sqft', 'region', 'zone'], inplace=True)
    df = df.loc[:,
         ['project_name', 'km_to_sg_cbd', 'lot_size_sqft', 'project_age', 'num_of_units', 'approx_project_gfa',
          'lot_size_num_units_ratio', 'avg_area_sqft', 'region', 'zone', 'lease_type', 'enbloc']]
    # impute mean
    for col in cols:
        df[col] = df[col].fillna(value=df[col].mean())
    # get dummies
    df = pd.get_dummies(df, columns=dummies)
    names = df.project_name
    ndf = df.loc[:, ['project_name', 'project_age', 'num_of_units', 'lot_size_num_units_ratio', 'approx_project_gfa']]
    df = df.drop('project_name', axis=1)
    enbloc = df.enbloc
    x = df.loc[:, df.columns != 'enbloc']
    return df, names, enbloc, x, ndf


def make_results(model_names, model_list, x, names, enbloc, ndf):
    pred = lambda model: pd.DataFrame(model.predict_proba(x)).iloc[:, 1]
    models = list(map(pred, model_list))

    data = {"project_name": pd.DataFrame(names.values),
            "enbloc": pd.DataFrame(enbloc.values),
            f"{model_names[0]}": models[0],
            f"{model_names[1]}": models[1],
            f"{model_names[2]}": models[2],
            f"{model_names[3]}": models[3],
            f"{model_names[4]}": models[4],
            f"{model_names[5]}": models[5],
            f"{model_names[6]}": models[6],
            f"{model_names[7]}": models[7],
            }

    results = pd.concat(data, axis=1)
    model_results = results.iloc[:, 2:]
    results['average'] = model_results.mean(numeric_only=True, axis=1)
    results.columns = results.columns.droplevel(1)
    results = results.sort_values('average', ascending=False)
    results['proportion'] = results.enbloc.sum() / results.shape[0]
    return results.merge(ndf, on='project_name', how='left')


def cluster(x, ndf, clusters, results):
    '''
    args:
    x:        pd.DataFrame. The df of x values.
    ndf:      pd.DataFrame. The df of property names. One of 'cndf', 'lndf'.
    clusters: int. Num of clusters.
    results:  pd.DataFrame. The df of the original results.

    '''
    x = x.loc[:, ['km_to_sg_cbd', 'lot_size_sqft', 'project_age', 'num_of_units', 'approx_project_gfa',
                  'lot_size_num_units_ratio', 'avg_area_sqft', 'lease_type_freehold', 'lease_type_leasehold']]
    x = PowerTransformer().fit_transform(x)
    x = pd.DataFrame(x, index=ndf.index)

    km = pd.Series(KMeans(n_clusters=clusters, random_state=0).fit_predict(x), index=ndf.project_name)
    aggl = pd.Series(AgglomerativeClustering(n_clusters=clusters).fit_predict(x), index=ndf.project_name)
    gmm = pd.Series(GaussianMixture(n_components=clusters, random_state=0).fit_predict(x), index=ndf.project_name)

    clusters = pd.concat({'km': km, 'aggl': aggl, 'gmm': gmm}, axis=1).reset_index()
    results = results.merge(clusters, on='project_name', how='left')

    group = lambda feature: results.groupby(feature)['enbloc'].transform('mean')

    results['kmprob'] = group('km')
    results['agglprob'] = group('aggl')
    results['gmmprob'] = group('gmm')
    results['cluster_average'] = (results['kmprob'] + results['agglprob'] + results['gmmprob']) / 3
    results['scaled_cluster_average'] = results['cluster_average'] / max(results[results.enbloc == 0]['cluster_average'])
    results['final_average'] = results['scaled_cluster_average'] * 0.1 + results['average'] * 0.9

    return results


cols = ['project_age', 'km_to_sg_cbd', 'lot_size_sqft', 'num_of_units', 'lot_size_num_units_ratio']
dummies = ['region', 'zone', 'lease_type']

cdf, cnames, cenbloc, cx, cndf = preprocess(condo_df, cols, dummies)
ldf, lnames, lenbloc, lx, lndf = preprocess(landed_df, cols, dummies)

fcx, flx = cx, lx
fcx['project_age'] = fcx['project_age'] + 3
flx['project_age'] = flx['project_age'] + 3
