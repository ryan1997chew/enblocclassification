import os
import pandas
from rea_python.main.database import DBCopyMode, RedshiftHook
from rea_python.constants import OutputFormat
import config as config

sql_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'sql')

# initialize redshift hook
src_hook = RedshiftHook(iam_role_arn=config.IAM_ROLE_ARN,
                        via_s3_bucket=config.REDSHIFT_HOOK_BRIDGE_BUCKET,
                        via_s3_folder=config.REDSHIFT_HOOK_BRIDGE_FOLDER)

# set up connection
src_hook.set_conn_from_uri(config.DATA_WAREHOUSE_URI)

# load queries from sql
src_hook.load_queries_from_folders([sql_path])


def query(query_str):
    df = src_hook.execute_loaded_query(query_name=query_str, output_format=OutputFormat.pandas)
    return df


def query_func():
    enbloc_df = query("enbloc")
    non_enbloc_df = query("non_enbloc")
    print(f"Number of rows in this Enbloc dataset is {enbloc_df.shape[0]}.")
    print(f"Number of rows in this Non-enbloc dataset is {non_enbloc_df.shape[0]}.")
    return enbloc_df, non_enbloc_df


enbloc_df, non_enbloc_df = query_func()

src_hook.copy_from_df(
    df=enbloc_df,
    target_table=f'data_science.enbloc_processed_enbloc_sg',
    mode=DBCopyMode.DROP
)

src_hook.copy_from_df(
    df=non_enbloc_df,
    target_table=f'data_science.enbloc_processed_non_enbloc_sg',
    mode=DBCopyMode.DROP
)

final_df = query("final")
src_hook.copy_from_df(
    df=final_df,
    target_table='data_science.enbloc_processed_final_sg',
    mode=DBCopyMode.DROP
)

print('Done')