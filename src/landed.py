

'''
This script will train landed
'''

import config as config
from rea_python.main.database import DBCopyMode, RedshiftHook
from rea_python.constants import OutputFormat
from pycaret.classification import *
import clean
from clean import ldf, lnames, lenbloc, lx, lndf, make_results, src_hook, cluster
import click

# TODO: wrap this in a function and use click. If 1, then normal. If 2, then transf/normalize. If 3, then transf/normalize/multicollinearity
@click.command()
@click.argument('session_id', required=True, type=int)
def main(session_id):
    if session_id == 1:
        lcaret = setup(data=ldf, target='enbloc', train_size=0.7, preprocess=False, fix_imbalance=True,data_split_stratify=True,
                       session_id=1)

    if session_id == 2:
        lcaret = setup(data=ldf, target='enbloc', train_size=0.7, preprocess=False, fix_imbalance=True,data_split_stratify=True,
                       transformation=True,
                       normalize=True,
                       normalize_method='robust',
                       session_id=2)

    if session_id == 3:
        lcaret = setup(data=ldf, target='enbloc', train_size=0.7, preprocess=False, fix_imbalance=True,data_split_stratify=True,
                       transformation=True,
                       normalize=True,
                       normalize_method='robust',
                       remove_multicollinearity=True,
                       session_id=3)

    l10 = compare_models(n_select=10, sort='AUC')
    lr = pull()
    lmodel_names = lr.index.values
    landed_results = make_results(lmodel_names, l10, lx, lnames, lenbloc, lndf)
    results = cluster(lx, lndf, 10, landed_results)
    results = results.round(decimals=4)

    src_hook.copy_from_df(
        df=results,
        target_table=f'data_science.enbloc_predictions_landed_{session_id}',
        mode=DBCopyMode.DROP
    )

if __name__ == '__main__':
    main()