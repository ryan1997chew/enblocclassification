'''
This script will train Condo
'''
import click
import config as config
from rea_python.main.database import RedshiftHook
from rea_python.constants import OutputFormat, DBCopyMode
from pycaret.classification import *
import clean
from clean import cdf, cnames, cenbloc, cx, cndf, make_results, src_hook, cluster


@click.command()
@click.argument('session_id', required=True, type=int)
def main(session_id):
    if session_id == 1:
        ccaret = setup(data=cdf, target='enbloc', train_size=0.7, preprocess=False, fix_imbalance=True, data_split_stratify=True,
                       session_id=1)

    if session_id == 2:
        ccaret = setup(data=cdf, target='enbloc', train_size=0.7, preprocess=False, fix_imbalance=True,data_split_stratify=True,
                       transformation=True,
                       normalize=True,
                       normalize_method='robust',
                       session_id=2)

    if session_id == 3:
        ccaret = setup(data=cdf, target='enbloc', train_size=0.7, preprocess=False, fix_imbalance=True,data_split_stratify=True,
                       transformation=True,
                       normalize=True,
                       normalize_method='robust',
                       remove_multicollinearity=True,
                       session_id=3)

    c10 = compare_models(n_select=10, sort='AUC')
    cr = pull()
    cmodel_names = cr.index.values
    condo_results = make_results(cmodel_names, c10, cx, cnames, cenbloc, cndf)
    results = cluster(cx, cndf, 15, condo_results)
    results = results.round(decimals=4)


    src_hook.copy_from_df(
        df=results,
        target_table=f'data_science.enbloc_predictions_condo_{session_id}',
        mode=DBCopyMode.DROP
    )

    print('Results written.')


if __name__ == '__main__':
    main()