"""
Description: This program will store the CSV in input to data_science. It's easier to do data cleaning in SQL than in Pandas.
"""
import os
import pandas as pd
import numpy as np
from rea_python.main.database import DBCopyMode, RedshiftHook
from rea_python.constants import OutputFormat
import config as config

src_hook = RedshiftHook(iam_role_arn=config.IAM_ROLE_ARN,
                        via_s3_bucket=config.REDSHIFT_HOOK_BRIDGE_BUCKET,
                        via_s3_folder=config.REDSHIFT_HOOK_BRIDGE_FOLDER)

src_hook.set_conn_from_uri(config.DATA_WAREHOUSE_URI)

def write(df, filename):
    src_hook.copy_from_df(
        df=df,
        target_table=f'data_science.enbloc_{filename}',
        mode=DBCopyMode.DROP
    )
    print("Table written.")
    return 1

data_directory = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), 'input')
filenames = os.listdir(data_directory)

raw_source_sg = pd.read_csv(fr"{data_directory}\{filenames[0]}")
raw_ura = pd.read_csv(fr"{data_directory}\{filenames[1]}")


#name cleaning
raw_ura.columns = raw_ura.columns.str.lower()
raw_ura.columns = raw_ura.columns.str.replace('\([^()]*\)','',regex=1).str.rstrip().str.replace(' ','_')
#replace
raw_ura = raw_ura.replace(['N.A.','N.A', 'NA','-',''] , np.nan)
raw_source_sg = raw_source_sg.replace(['UNKNOWN','N.A.','N.A','NA',''] , np.nan)

#write to data_science
write(raw_source_sg,'raw_source_sg')
write(raw_ura,'raw_ura')



