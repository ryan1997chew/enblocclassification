# Project description

This project is to predict en bloc probability - i.e the probability a developer will approach the project to buy it out. Does it work?

Unfortunately, the problem is that unlike a normal binary classification problem, the label `0` may not always be the true label. In other words, a currently non-enbloc project in the REA DBs may be a `1` from the perspective of a developer.

There were 2 approaches: one using the vanilla classification models, and one using a clustering approach to naively determine probability of enbloc. Take results with pinch of salt. For full process, check out the notebooks.
## Viewing results
Result tables are in `data_science.enbloc_prediction_{property_type}_{setup number}`.
If you want to export to excel for viewing:

Run `get_results_from_DB.py`. This will populate the Excel book in `input/enbloc_predictions.xlsx`

Average is the avg probability of the PyCaret models. 
Cluster average =  𝑝(𝑒𝑛𝑏𝑙𝑜𝑐|𝑐𝑙𝑢𝑠𝑡𝑒𝑟)  = ratio of nonenblocs to enblocs in said cluster.
Final average = 0.1 * Scaled Cluster Avg + 0.9 * Average.
Correlation coefficient between average and cluster_average is 0.62.

It is worth noting the probabilities are meant to be viewed relatively (in relation to one another). Their absolute scale can be changed quite easily by tuning the model parameters. 

## Running the project 
```bash
pip install requirements.txt
(install visual-studio build tools --> install desktop development toolkit --> this is for PyCaret - takes very long)
pip install pycaret
```
Files to run:
1. write_to_db_original.py (generates raw tables, the enbloc csv in input)
2. write_to_db_processed.py (generates processed tables, from raw tables)
3. condo.py n 
4. landed.py n

Where n={1,2,3}
* Session ID = 1 - no transformation and no scaling.
* Session ID = 2 - transformation and scaling.
* Session ID = 3 - transformation and scaling and remove multicollinearity.

The output is `data_science.enbloc_prediction_condo_{n}`. It shows predictions for all the models PyCaret ran,
sorted by the highest AUC. It also shows the average prediction probability, across all models used.

## Interesting features to add 

Due to data reasons can't rly get these features. sed.
1. AUR - area utilization ratio. AUR = Actual GFA/Maximum GFA. 

Max GFA = GPR x L (Max gross floor area = gross plot ratio x lot size)

Actual GFA = actual gross floor area.

However we need plot ratio, we don't have that yet.

2. Change in DC (development charge)

Development charge is a tax levied by URA on developers who want to enbloc the project. URA releases annual DC changes or semiannual. The idea is to track the relative change in DC for the past two or one year. 
If DCs go down in that area that in theory could entice developers to enbloc the place.

3. Relative area valuation

The idea is that if your area is going up in value then developers want to enbloc your place too. 

These 3 features may be interesting but hard to find data.

